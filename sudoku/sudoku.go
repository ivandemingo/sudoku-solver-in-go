package sudoku

type Sudoku struct {
    puzzle [9][9]int
}

func New(puzzle [9][9]int) *Sudoku {
    return &Sudoku{puzzle}
}

func (s *Sudoku) Get(row, col int) int {
    return s.puzzle[row][col]
}

func (s *Sudoku) Set(row, col, v int) {
    s.puzzle[row][col] = v
}

func (s *Sudoku) PossibleValues(row, col int) []int {
    var rowExistingValues []int
    for i := 0; i < 9; i++ {
        v := s.puzzle[row][i]
        if v != 0 {
            rowExistingValues = append(rowExistingValues, v)
        }
    }

    var colExistingValues []int
    for i := 0; i < 9; i++ {
        v := s.puzzle[i][col]
        if v != 0 {
            colExistingValues = append(colExistingValues, v)
        }
    }

    firstNonetRow := (row / 3) * 3
    firstNonetCol := (col / 3) * 3

    var nonetExistingValues []int
    for i := firstNonetRow; i < firstNonetRow + 3; i++ {
        for j := firstNonetCol; j < firstNonetCol + 3; j++ {
            v := s.puzzle[i][j]

            if v != 0 {
                nonetExistingValues = append(nonetExistingValues, v)
            }
        }
    }

    var existingValues []int
    existingValues = append(existingValues, rowExistingValues...)
    existingValues = append(existingValues, colExistingValues...)
    existingValues = append(existingValues, nonetExistingValues...)

    var pv []int
    var exists bool
    for i := 1; i < 10; i++ {
        exists = false
        for _, existingValue := range existingValues {
            if existingValue == i {
                exists = true
                break
            }
        }

        if !exists {
            pv = append(pv, i)
        }
    }

    return pv
}
