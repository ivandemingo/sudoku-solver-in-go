package main

import (
    "errors"
    . "fmt"
    "sudoku-resolver/sudoku"
)

func main() {
    Println("SUDOKU SOLVER")

    sdk := initialSudoku()

    Println("Initial Sudoku:")
    printSudoku(sdk)

    Println("Solving Sudoku...")

    rs, err := solve(*sdk, 0, 0)

    if nil != err {
        Println(err.Error())
        return
    }

    Println("Solved Sudoku:")
    printSudoku(&rs)
}

func solve(sdk sudoku.Sudoku, row, col int) (sudoku.Sudoku, error) {
    v := sdk.Get(row, col)

    if v != 0 {
        if row == 8 && col == 8 {
            return sdk, nil
        }

        if col == 8 {
            row++
        }

        return solve(sdk, row, (col + 1) % 9)
    }

    pv := sdk.PossibleValues(row, col)

    for _, v := range pv {
        sdk.Set(row, col, v)

        if row == 8 && col == 8 {
            return sdk, nil
        }

        if col == 8 {
            row++
        }

        rs, err := solve(sdk, row, (col + 1)%9)

        if nil == err {
            return rs, nil
        }
    }

    return sdk, errors.New("invalid Sudoku")
}

func initialSudoku() *sudoku.Sudoku {
    puzzle := [9][9]int{
        {0,0,5,0,0,0,0,2,0},
        {7,0,0,0,4,0,0,1,8},
        {6,0,0,0,0,0,0,0,9},
        {0,0,7,0,9,0,5,0,3},
        {0,0,0,7,0,0,0,0,4},
        {0,0,6,8,0,0,0,0,0},
        {0,0,2,0,0,6,0,0,0},
        {4,0,0,0,0,0,0,0,0},
        {3,8,1,0,0,0,0,7,0},
    }

    return sudoku.New(puzzle)
}

func printSudoku(sdk *sudoku.Sudoku) {
    for i := 0; i < 9; i++ {
        if i > 0 && i % 3 == 0 {
            Println("---------------------")
        }
        for j := 0; j < 9; j++ {
            if j > 0 && j % 3 == 0 {
                Print("| ")
            }
            Print(sdk.Get(i, j))
            Print(" ")
        }
        Print("\n")
    }
}
